def call(dockerRepoName, imageName) {
    pipeline {
        agent any
        environment {
            PATH = "/var/lib/jenkins/.local/bin:$PATH"
        }
        stages {
            stage('Before Build') {
                steps {
                    sh 'python3 -m venv bandit-env'
                    sh 'chmod +x ./bandit-env/bin/activate'
                    sh './bandit-env/bin/activate && pip install bandit'
                }
            }
            stage('Pylint') {
                steps {
                    sh """
                    ./bandit-env/bin/activate
                    pip install pylint==3.1.0
                    python3 -m pylint --disable import-error --fail-under=5  ${dockerRepoName}/*.py
                    """        
                }
            }
            stage('Security') {
                steps {
                    sh """
                    pip install bandit
                    python3 -m bandit -r ${dockerRepoName}/*.py
                    """
                }
            }
            stage('Package') {
                when {
                    expression { env.GIT_BRANCH == 'origin/main' }
                }
                steps {
                    withCredentials([string(credentialsId: 'munibj', variable: 'TOKEN')]) {
                        sh "docker login -u 'munibj' -p '$TOKEN' docker.io"
                        sh "docker build -t ${dockerRepoName.toLowerCase()}:latest --tag munibj/${dockerRepoName.toLowerCase()}:${imageName} ./${dockerRepoName}"
                        sh "docker push munibj/${dockerRepoName.toLowerCase()}:${imageName}"
                    }
                }
            }
            stage("Deploy") {
                steps {
                    sshagent(credentials: ['ssh-agent']) {
                        sh """
                        ssh -t -t ubuntu@ec2-35-174-235-217.compute-1.amazonaws.com -o StrictHostKeyChecking=no \
                        "cd /home/ubuntu/ass3_integration && pwd \
                        && docker login \
                        && docker compose pull ${dockerRepoName.toLowerCase()} \
                        && docker compose up --build -d"
                        """
                    }   
                }
            }
        }
        post {
            success {
                echo 'Pipeline succeeded!'
            }
            failure {
                echo 'Pipeline failed!'
            }
            always {
                echo 'Pipeline finished.'
            }
        }
    }
}
